def digit_counter(n):
    for d in n:
        if not d.isdigit():
            return -1
    return len(n)