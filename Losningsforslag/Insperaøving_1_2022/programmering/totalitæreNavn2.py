tidlig = ["mandag", "tirsdag", "onsdag",]
tidligeNavn = ["Magnus", "Marie"]
sent = ["torsdag", "fredag", "lørdag", "søndag"]
seneNavn = ["Ola", "Karoline"]

ukedag = input("Hvilken ukedag er det? ").lower()
navn = input("Hvliket navn har du valgt? ")

if (ukedag in tidlig and navn in tidligeNavn) or (ukedag in sent and navn in seneNavn) or (ukedag == "lørdag" and len(navn) == 4):
    print("Det er lovlig")
else:
    print("Det er ulovlig")
