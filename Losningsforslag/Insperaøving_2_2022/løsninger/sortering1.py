# Med sorted()
def sorterEtterNavn(personer):
    return sorted(personer)
    
# Med list.sort()
def sorterEtterNavn(personer):
    personerCopy = personer.copy()
    personerCopy.sort()
    return personerCopy
