# Med sample
def tilfeldigListe(l, k):
    return sample(l, k)

# Med forrige løsning
def tilfeldigListe(l, k):
    liste = l.copy()
    output = []
    for i in range(k):
        valg = tilfeldigValg(liste)
        output.append(valg)
        liste.remove(valg)
    return output