def førsteBokstaver(s):
    letters = []
    for word in s.split(" "):
        letters.append(word[0])
    return letters
